import Vue from 'vue';
Vue.config.productionTip = false;
import '../src/base-main.js';
import App from '../src/pages/index/index.vue';
new Vue({ render: h => h(App) }).$mount('#app'); 