import Vue from 'vue';
Vue.config.productionTip = false;
import '../src/base-main.js';
import App from '../src/pages/about/about.vue';
import router from '../src/pages/about/router/index.js';
new Vue({ router , render: h => h(App) }).$mount('#app'); 