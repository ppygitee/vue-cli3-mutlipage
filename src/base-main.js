/** 
* @description: 存放公共依赖
*/

import Vue from 'vue'
// import Vant from 'vant';
// import 'vant/lib/index.css';
// Vue.use(Vant)

// import requests from '@s/basejs/new-axios.js'
// Vue.prototype.rq = requests


// 通用工具库
// import {tools} from '@/utils/tools.js'
// Vue.prototype.tools = tools;

import '@/assets/styles/main.less';
import '@/assets/styles/base.less';

// 全局过滤器
import * as filters from '@/utils/filters.js'
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})
