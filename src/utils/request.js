import axios from "axios";
import qs from "qs";

const service = axios.create({
  // baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 5000, // request timeout
});

service.interceptors.request.use(
  (config) => {
    if (config.method === "post") {
      const contentType = config.headers["Content-Type"];
      if (contentType && contentType.indexOf("multipart/form-data") === 0) {
      } else {
        config.data = qs.stringify(config.data, {
          arrayFormat: "repeat",
          allowDots: true,
        });
      }
    }
    return config;
  },
  (error) => {
    console.log("interceptors", error); // for debug
    return Promise.reject(error);
  }
);

service.interceptors.response.use(
  (response) => {
    // console.log('response', response) // for debug
    return response.data;
  },
  (error) => {
    // console.log('response error', error.status) // for debug
    Dialog.alert({
      message: error.data.msg,
    }).then(() => {
      return Promise.reject(error);
    });
  }
);

export default service;
