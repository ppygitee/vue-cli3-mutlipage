# vue-cli3-mutlipage

#### 介绍
基于vue-cli3搭建的多页面模板

可以根据.vue文件自动生成多页面
#### 安装教程

1.  npm install
2.  npm run serve
3.  npm run build

#### 使用说明

-config/
  -package.json => 用于配置每个页面的标题
-entry/
  -x.js  =>入口文件, 打包时自动生成
-public/ =>存放html模板文件
-src/ 
  -assets => 资源
  -components => 公共组件
  -pages => 多页面
  -store => vuex
  -utils => 工具库
-package.json => 依赖
-vue.config.js => 配置文件

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
