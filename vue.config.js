const path = require('path');
const fs = require('fs');
const glob = require('glob');

const targetUrl = "http://"; // 代理地址
const port = 8888; // 运行端口
const headers = {};

// 配置选项
const config = {
  outputDir: "dist",
  publicPath: '/', // 默认'/'，部署应用包时的基本 URL // publicPath: process.env.NODE_ENV === "production" ? "./" : "./",
  lintOnSave: false,
  pages: Object.assign(getPages(),
    //   {app: './src/main.js'    // 配置主入口文件（会生成 app.html，vue cli3并没有提供直接配置入口文件的选项）
    // }
  ),
  devServer: {
    host: "0.0.0.0",
    port: port,
    open: true, // 启动后是否自动打开网页
    proxy: {
      "/xx": {
        target: targetUrl,
        changeOrigin: true,
        headers: headers,
        pathRewrite: {
          "^/xx": "/xx"
        }
      },
    },
    disableHostCheck: true
  }
}

// 获取多页面的配置数据
function getPages () {
  const pages = {};
  glob.sync('./src/pages/*/*.vue').forEach(function (pageUrl) {
    const ext = path.extname(pageUrl);
    const pageCode = path.basename(pageUrl, ext);
    if (pages[pageCode]) {
      console.error(`文件名不能重复使用：${pageCode}。\n`);
      process.exit(1);
    }
    // 生成入口文件
    const entryFile = `./entry/${pageCode}.js`;


    fs.exists(entryFile, async function (exists) {
      if (exists) return;
      // 创建文件及写入文件内容
      const appTpl = '.' + pageUrl;
      let entryData = ''
      // 判断是否有路由文件
      await fs.exists(`./src/pages/${pageCode}/router/index.js`, function (e) {
        if (e) {
          entryData = `import Vue from 'vue';\nVue.config.productionTip = false;\nimport '../src/base-main.js';\nimport App from '${appTpl}';\nimport router from '../src/pages/${pageCode}/router/index.js';\nnew Vue({ router , render: h => h(App) }).$mount('#app'); `;
          fs.writeFile(entryFile, entryData, function (err) {
            // err.code === 'ENOENT'
            if (err) console.log(err);
          });
        }
        if (!e) {
          entryData = `import Vue from 'vue';\nVue.config.productionTip = false;\nimport '../src/base-main.js';\nimport App from '${appTpl}';\nnew Vue({ render: h => h(App) }).$mount('#app'); `;
          fs.writeFile(entryFile, entryData, function (err) {
            // err.code === 'ENOENT'
            if (err) console.log(err);
          });
        }
      })
    });
    // 自定义页面数据，用于增加附加数据，例如title等。
    const pagesJson = require('./config/page.json');
    const pageData = pagesJson[pageCode] || {};
    Object.assign(pageData, {
      url: pageUrl,
      code: pageCode
    });
    // 配置多页面
    pages[pageCode] = {
      entry: entryFile,               // 入口文件
      template: './public/index.html',         // 模板文件
      filename: pageCode + '.html',   // 打包后的文件路径
      minify: false,                  // 是否压缩
      chunks: ['chunk-vendors', 'chunk-common', 'app', pageCode],   // 引入资源文件
      chunksSortMode: 'manual',       // 控制 chunk 的排序。none | auto（默认）| dependency（依赖）| manual（手动）| {function}
      title: pageData.title,  // 标题
      code: pageData.code
    };
  });
  return pages;
}

module.exports = config;